﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SkmPresentation
{
    [TestFixture]
    public class HtmlAgilityPackTests
    {
        private const string SingleDocument = "SingleDocument.html";
        private const string BigTable = "BigTable.html";
        private const string PartialHtml = "PartialHtml.html";

        private HtmlNode LoadDocument(string filePath)
        {
            var document = new HtmlDocument();
            document.Load(new FileInfo(filePath).Name);
            return document.DocumentNode;
        }

        [Test]
        public void Extract_Nodes_From_Html()
        {
            var document = LoadDocument(SingleDocument);

            var paragraphNodes = document.SelectNodes("//p");
            Assert.AreEqual(3, paragraphNodes.Count);
        }

        [Test]
        public void Get_Text_Of_Node_From_Html()
        {
            var document = LoadDocument(SingleDocument);

            var testClassParagraph = document.SelectSingleNode("//p[contains(@class, 'test-class')]");
            Assert.AreEqual("My test class paragraph.", testClassParagraph.InnerText);
        }

        [Test]
        public void Extract_Text_Node()
        {
            var document = LoadDocument(SingleDocument);
            var hackedDiv = document.SelectSingleNode("//div[@id='hack']/text()");
            Assert.AreEqual("hacker's hack", hackedDiv.InnerText.Trim());

            // Selenium? javascript? Other ideas?
        }

        [Test]
        public void Extract_Hidden_Text()
        {
            // HtmlAgilityPack
            { 
                var document = LoadDocument(SingleDocument);
                var invisibleTextNode = document.SelectSingleNode("//div[@id='hidden-text']");
                Assert.AreEqual("hidden text", invisibleTextNode.InnerText.Trim());
            }
            // Selenium?
            {
                var driver = new ChromeDriver();
                driver.Navigate().GoToUrl(new Uri(new FileInfo(SingleDocument).FullName).AbsoluteUri);
                var invisibleTextNode = driver.FindElement(By.XPath("//div[@id='hidden-text']"));
                Assert.AreEqual("", invisibleTextNode.Text);
                driver.Quit();
            }
        }

        [Test]
        public void More_Efficient_Way_To_Parse_Large_Set_Of_Nodes()
        {
            // traditional, WebDriver way
            {
                var tableCellValues = new List<string>();

                var stopwatch = new Stopwatch();
                var driver = new ChromeDriver();
                driver.Navigate().GoToUrl(new Uri(new FileInfo(BigTable).FullName).AbsoluteUri);

                stopwatch.Start();
                var table = driver.FindElement(By.XPath(".//table[@id = 'test-table']"));
                foreach (var row in table.FindElements(By.XPath("./tbody/tr")))
                {
                    var cells = row.FindElements(By.XPath("./td"));
                    foreach (var cell in cells)
                    {
                        tableCellValues.Add(cell.Text);
                    }
                }
                stopwatch.Start();
                Console.WriteLine("Selenium table parsing: {0}", stopwatch.ElapsedMilliseconds);
                driver.Quit();
            }

            // Using HtmlAgilityPack
            {
                var tableCellValues = new List<string>();

                var stopwatch = new Stopwatch();
                var tableDocument = LoadDocument(BigTable);

                stopwatch.Start();
                var table = tableDocument.SelectSingleNode(".//table[@id = 'test-table']");
                foreach (var row in table.SelectNodes("./tbody/tr"))
                {
                    var cells = row.SelectNodes("./td");
                    foreach (var cell in cells)
                    {
                        tableCellValues.Add(cell.InnerText);
                    }
                }
                stopwatch.Stop();
                Console.WriteLine("HtmlAgilityPack table parsing: {0}", stopwatch.ElapsedMilliseconds);
            }
        }

        [Test]
        public void Load_More_Than_One_Space()
        {
            {
                // Using HtmlAgilityPack
                var partialDocument = LoadDocument(PartialHtml);
                var testHarnessNode = partialDocument.SelectSingleNode("//div[@id='test-harness']");
                Assert.AreEqual("test   harness", testHarnessNode.InnerText);
            }
            {
                var driver = new ChromeDriver();
                driver.Navigate().GoToUrl(new FileInfo(PartialHtml).FullName);
                var testHarnessNode = driver.FindElement(By.XPath("//div[@id='test-harness']"));
                Assert.AreEqual("test harness", testHarnessNode.Text);
                driver.Quit();
            }
        }
    }
}
