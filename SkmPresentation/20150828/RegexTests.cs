﻿using System.Text.RegularExpressions;
using NUnit.Framework;

namespace SkmPresentation._20150828
{
    [TestFixture]
    public class RegexTests
    {
        [Test]
        public void Match_Characters()
        {
            var regex = new Regex("abc");
            Assert.IsTrue(regex.IsMatch("abc"));

            Assert.IsFalse(regex.IsMatch("bbcd"));
            Assert.IsFalse(regex.IsMatch("a"));
        }

        [Test]
        public void Match_Many_Occurences()
        {
            var regex = new Regex("abc");
            Assert.IsTrue(regex.IsMatch("ds aiu djsaoudh soaijd -0iqw 9hywq !!! abc !!! osduha doi sha iud hwqu"));
            Assert.IsTrue(regex.IsMatch("abc sdiuha odh iudhgs isuha abc"));

            Assert.IsFalse(regex.IsMatch("ab uidh saijhdsoiua bc odsjh sioajd oas c"));
        }

        [Test]
        public void Match_Single_Character_Test()
        {
            var regex = new Regex("abc.");
            Assert.IsTrue(regex.IsMatch("abcd"));
            Assert.IsTrue(regex.IsMatch("abcb"));
            Assert.IsTrue(regex.IsMatch("abc6"));
            Assert.IsTrue(regex.IsMatch("abc!"));
            Assert.IsTrue(regex.IsMatch("abc "));
            
            Assert.IsFalse(regex.IsMatch("cdaz"));
            Assert.IsFalse(regex.IsMatch("defg"));
        }

        [Test]
        public void Match_Non_Special_Character()
        {
            var regex = new Regex(@".bc\.");
            Assert.IsTrue(regex.IsMatch("abc."));
            Assert.IsTrue(regex.IsMatch("bbc."));

            Assert.IsFalse(regex.IsMatch("abca"));
            Assert.IsFalse(regex.IsMatch("bbcb"));
        }

        [Test]
        public void Match_Many_Characters_Test()
        {
            var regex = new Regex("abc.+");
            Assert.IsTrue(regex.IsMatch("abcd"));
            Assert.IsTrue(regex.IsMatch("abcde"));
            Assert.IsTrue(regex.IsMatch("abcde1"));
            Assert.IsTrue(regex.IsMatch("abcde1! "));

            Assert.IsFalse(regex.IsMatch("abc"));
            Assert.IsFalse(regex.IsMatch("a"));
        }

        [Test]
        public void Match_Possible_Characters()
        {
            var regex = new Regex("ab[cd]");
            Assert.IsTrue(regex.IsMatch("abc"));
            Assert.IsTrue(regex.IsMatch("abd"));

            Assert.IsFalse(regex.IsMatch("abedcdcd"));
            Assert.IsFalse(regex.IsMatch("ab1"));
            Assert.IsFalse(regex.IsMatch("ab!"));
        }

        [Test]
        public void Match_Many_Possible_Characters()
        {
            var regex = new Regex("ab[cd]+");
            Assert.IsTrue(regex.IsMatch("abc"));
            Assert.IsTrue(regex.IsMatch("abd"));
            Assert.IsTrue(regex.IsMatch("abcdcdcd"));
            Assert.IsTrue(regex.IsMatch("abdddddd"));
            Assert.IsTrue(regex.IsMatch("abcccddd"));

            Assert.IsFalse(regex.IsMatch("ab1cdcdcd"));
            Assert.IsFalse(regex.IsMatch("ab!dddccc"));
        }

        [Test]
        public void Match_Ranges_Test()
        {
            var regex = new Regex("ab[a-z]");
            Assert.IsTrue(regex.IsMatch("abc"));
            Assert.IsTrue(regex.IsMatch("abd"));
            Assert.IsTrue(regex.IsMatch("abz"));

            Assert.IsFalse(regex.IsMatch("ab1"));
            Assert.IsFalse(regex.IsMatch("ab!"));
        }

        [Test]
        public void Match_Number_Ranges()
        {
            var regex = new Regex("ab[a-z0-9]");
            Assert.IsTrue(regex.IsMatch("abc"));
            Assert.IsTrue(regex.IsMatch("ab1"));

            Assert.IsFalse(regex.IsMatch("ab!"));
            Assert.IsFalse(regex.IsMatch("ab!"));
        }

        [Test]
        public void Match_Ranges_With_Many_Characters()
        {
            var regex = new Regex("ab[a-z0-9]+");
            Assert.IsTrue(regex.IsMatch("abcdefghi"));
            Assert.IsTrue(regex.IsMatch("ab123456"));
            Assert.IsTrue(regex.IsMatch("abcde123"));

            Assert.IsFalse(regex.IsMatch("ab!$1A"));
        }



        [Test]
        public void Match_Spaces_Test()
        {
            var regex = new Regex(@"abc\sdef");
            Assert.IsTrue(regex.IsMatch("abc def"));
            Assert.IsTrue(regex.IsMatch("abc\tdef"));

            Assert.IsFalse(regex.IsMatch("abcdef"));
            Assert.IsFalse(regex.IsMatch("abc1def"));
            Assert.IsFalse(regex.IsMatch("abc!def"));

            // but...
            regex = new Regex(@"abc.def");
            Assert.IsTrue(regex.IsMatch("abc def")); // !!
        }

        [Test]
        public void Match_Negation_Range()
        {
            var regex = new Regex("ab[^def]+");
            Assert.IsTrue(regex.IsMatch("abc"));
            Assert.IsTrue(regex.IsMatch("abcgh"));

            Assert.IsFalse(regex.IsMatch("abd"));
            Assert.IsFalse(regex.IsMatch("abeee"));
            Assert.IsFalse(regex.IsMatch("abefd"));
        }

        [Test]
        public void Match_First_Group()
        {
            var regex = new Regex("ab(.+)");

            var match = regex.Match("abcdef");
            Assert.AreEqual("abcdef", match.Groups[0].Value);

            match = regex.Match("ab12345");
            Assert.AreEqual("ab12345", match.Groups[0].Value);
        }

        [Test]
        public void Match_Group()
        {
            var regex = new Regex("ab(.+)");

            var match = regex.Match("abcdef");
            Assert.AreEqual("cdef", match.Groups[1].Value);

            match = regex.Match("ab12345");
            Assert.AreEqual("12345", match.Groups[1].Value);
        }

        [Test]
        public void Or_Group()
        {
            var regex = new Regex("My name is (Janek|Katarzyna|Łukasz|(User[0-9]+))");
            Assert.IsTrue(regex.IsMatch("My name is Janek"));
            Assert.IsTrue(regex.IsMatch("My name is Katarzyna"));
            Assert.IsTrue(regex.IsMatch("My name is User12345"));
            Assert.AreEqual("Janek", regex.Match("My name is Janek").Groups[1].Value);
            Assert.IsTrue(regex.IsMatch("My name is Janek and Tomasz"));

            Assert.IsFalse(regex.IsMatch("My name is Tomasz"));
            Assert.IsFalse(regex.IsMatch("My name is Krzesimir"));
            Assert.IsFalse(regex.IsMatch("My name is UserABC123"));
            Assert.IsFalse(regex.IsMatch("My name is Tomasz"));
        }

        [Test]
        public void Not_Grouping_Group()
        {
            var regex = new Regex("this is (?:dog|cat) number ([0-9]+)");

            var match = regex.Match("this is dog number 1");
            Assert.AreNotEqual("dog", match.Groups[1].Value);
            Assert.AreEqual("1", match.Groups[1].Value);
        }

        [Test]
        public void Match_Start_Of_String()
        {
            var regex = new Regex("^abc.+");
            Assert.IsTrue(regex.IsMatch("abcdef"));
            Assert.IsTrue(regex.IsMatch("abcd"));

            Assert.IsFalse(regex.IsMatch(" oidsja oisaj abcdef"));
        }

        [Test]
        public void Match_End_Of_String()
        {
            var regex = new Regex(".+abc$");

            Assert.IsTrue(regex.IsMatch(" dsha duisa abc"));
            Assert.IsTrue(regex.IsMatch("test abc"));

            Assert.IsFalse(regex.IsMatch("abc test test"));
            Assert.IsFalse(regex.IsMatch("test abc test test"));
        }

        [Test]
        public void Real_Life_Example_Collection_Day()
        {
            var regex = new Regex("^([0-9]+)(?:st|nd|rd|th) ([A-Za-z]+)$");

            var match = regex.Match("21st Monthly");
            Assert.IsTrue(match.Success);
            Assert.AreEqual("21", match.Groups[1].Value);
            Assert.AreEqual("Monthly", match.Groups[2].Value);
        }

        [Test]
        public void Real_Life_Example_Charge_Payment()
        {
            var regex = new Regex(@"^InstalmentNo: ([0-9]+), ChargeType: ([ A-Za-z]+), Amount: £([0-9\.]+)$");
            var match = regex.Match("InstalmentNo: 10, ChargeType: Fixed Default Fee, Amount: £30.00");
            Assert.IsTrue(match.Success);
            Assert.AreEqual("10", match.Groups[1].Value);
            Assert.AreEqual("Fixed Default Fee", match.Groups[2].Value);
            Assert.AreEqual("30.00", match.Groups[3].Value);
        }

        [Test]
        public void Real_Life_Example_Log_Parsing()
        {
            var collectionDayRegex = new Regex(@"^Manual payment of (?<valueWithFee>[0-9\.\,]+) applied. " +
                @"Paid off instalment: #(?<instalmentNumber>[0-9]+)=(?<valueWithoutFee>[0-9\.\,]+) " +
                @"Merchant Ref:(?<merchantRef>[0-9A-Z\-_]+) " + 
                @"Auth:(?<authRef>[0-9A-Z]+)$");
            var text = "Manual payment of 307.69 applied. Paid off instalment: #8=301.66 Merchant Ref:02298600-013-008-M_20150824075439 Auth:789DE";
            var match = collectionDayRegex.Match(text);
            Assert.AreEqual("307.69", match.Groups["valueWithFee"].Value);
            Assert.AreEqual("8", match.Groups["instalmentNumber"].Value);
            Assert.AreEqual("301.66", match.Groups["valueWithoutFee"].Value);
            Assert.AreEqual("02298600-013-008-M_20150824075439", match.Groups["merchantRef"].Value);
            Assert.AreEqual("789DE", match.Groups["authRef"].Value);
        }
    }
}
