﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using NUnit.Framework;
using RestSharp;

namespace SkmPresentation
{
    [TestFixture]
    class RestSharpTests
    {
        [Test]
        public void Perform_Simple_Request()
        {
            var client = new RestClient("http://goyello.com/");
            var request = new RestRequest("pl/oferty-pracy-polska");
            var response = client.Get(request);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var responseContent = response.Content;
            StringAssert.Contains("Java Developer", responseContent);

            var document = new HtmlDocument();
            document.LoadHtml(responseContent);
            var currentJobOffers =
                "//div[contains(@class, 'flex_column') and not(contains(@class, 'first'))]//section[@class = 'av_textblock_section']//p";
            foreach (var currentJobOffer in document.DocumentNode.SelectNodes(currentJobOffers))
            {
                Console.WriteLine(currentJobOffer.InnerText.Trim());
            }
        }

        [Test]
        public void Perform_Get_Request()
        {
            var client = new RestClient("http://blog.goyello.com/");
            var request = new RestRequest();

            request.AddParameter("s", "test");

            var response = client.Get(request);
            
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var responseContent = response.Content;

            var document = new HtmlDocument();
            document.LoadHtml(responseContent);

            foreach (var blogNoteTitle in document.DocumentNode.SelectNodes("//h3"))
            {
                Console.WriteLine(blogNoteTitle);
            }
        }

        [Test]
        public void Perform_Post_Request()
        {
            var client = new RestClient("http://httpbin.org/");
            var request = new RestRequest("/post");
            request.AddParameter("test", "SKM rulez");
            var response = client.Post(request);
            
            StringAssert.Contains("\"test\": \"SKM rulez\"", response.Content);
        }
    }
}
